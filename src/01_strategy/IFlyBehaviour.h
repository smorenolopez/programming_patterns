//
// Created by sergio on 1/12/18.
//

#ifndef DESIGN_PATTERNS_IFLYBEHAVIOUR_H
#define DESIGN_PATTERNS_IFLYBEHAVIOUR_H

class IFlyBehaviour
{
public:
    virtual ~IFlyBehaviour() {};

    virtual void fly() = 0;
};

#endif //DESIGN_PATTERNS_IFLYBEHAVIOUR_H
