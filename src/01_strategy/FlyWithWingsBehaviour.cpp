//
// Created by sergio on 1/12/18.
//

#include <iostream>
#include "FlyWithWingsBehaviour.h"


void FlyWithWingsBehaviour::fly()
{
    std::cout << text << std::endl;
}

FlyWithWingsBehaviour::FlyWithWingsBehaviour() {

}

FlyWithWingsBehaviour::~FlyWithWingsBehaviour() {

}

void FlyWithWingsBehaviour::set(std::string s)
{
    text = s;
}