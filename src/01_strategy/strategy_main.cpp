#include <iostream>
#include "FlyWithWingsBehaviour.h"
#include "Duck.h"

class A;


int main()
{
    FlyWithWingsBehaviour flyBehaviour;
    flyBehaviour.set( "Flying with wings right now...");
    flyBehaviour.fly();

    Duck duck(flyBehaviour);
    duck.fly();

    return 0;
}

