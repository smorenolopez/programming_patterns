//
// Created by sergio on 1/12/18.
//

#ifndef DESIGN_PATTERNS_DUCK_H
#define DESIGN_PATTERNS_DUCK_H

#include "IFlyBehaviour.h"
#include <string>



class Duck
{
public:
    Duck(IFlyBehaviour& flyBehaviour) : m_flyBehaviour(flyBehaviour)
   {

    };
    ~Duck() {};

    void fly()
    {
        m_flyBehaviour.fly();
    }

    void quack();

private:
    IFlyBehaviour &m_flyBehaviour;

};

#endif //DESIGN_PATTERNS_DUCK_H
