//
// Created by sergio on 1/12/18.
//

#ifndef DESIGN_PATTERNS_FLYWITHWINGSBEHAVIOUR_H
#define DESIGN_PATTERNS_FLYWITHWINGSBEHAVIOUR_H


#include "IFlyBehaviour.h"
#include <string>

class FlyWithWingsBehaviour : public IFlyBehaviour
{
public:
    FlyWithWingsBehaviour();
    void set(std::string);
    ~FlyWithWingsBehaviour();

    void fly();

private:
    std::string text;
};


#endif //DESIGN_PATTERNS_FLYWITHWINGSBEHAVIOUR_H
