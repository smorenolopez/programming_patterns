//
// Created by sergio on 2/12/18.
//

#include <algorithm>

#include "WeatherStationObservable.h"


WeatherStationObservable::~WeatherStationObservable() = default;

void WeatherStationObservable::add(IObserver *ptr_observer)
{
    this->m_observers.push_back(ptr_observer);
}

void WeatherStationObservable::remove(IObserver *ptr_observer)
{
    std::vector<IObserver*>::iterator it;

    if ((it = std::find(m_observers.begin(), m_observers.end(), ptr_observer)) != m_observers.end())
    {
        this->m_observers.erase(it);
    }
}

void WeatherStationObservable::notify()
{
    for (auto ptr_observer : m_observers)
    {
        ptr_observer->update();
    }
}

int WeatherStationObservable::getTemperature()
{
    return m_temperature;
}

void WeatherStationObservable::setTemperature(int temperature)
{
    m_temperature = temperature;
}



