//
// Created by sergio on 2/12/18.
//

#ifndef DESIGN_PATTERNS_WEATHERSTATIONOBSERVABLE_H
#define DESIGN_PATTERNS_WEATHERSTATIONOBSERVABLE_H

#include <vector>

#include "IObservable.h"

class WeatherStationObservable : public IObservable
{
public:
    virtual ~WeatherStationObservable();

    void add(IObserver* ptr_observer);
    void remove(IObserver* ptr_observer);
    void notify();

    int getTemperature();
    void setTemperature(int temperature);

private:
    std::vector<IObserver*> m_observers;
    int m_temperature;
};


#endif //DESIGN_PATTERNS_WEATHERSTATIONOBSERVABLE_H
