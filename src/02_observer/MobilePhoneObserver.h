//
// Created by sergio on 2/12/18.
//

#ifndef DESIGN_PATTERNS_MOBILEPHONEOBSERVER_H
#define DESIGN_PATTERNS_MOBILEPHONEOBSERVER_H

#include "IObserver.h"
#include "WeatherStationObservable.h"


class MobilePhoneObserver : public IObserver
{
public:
    MobilePhoneObserver(WeatherStationObservable *ptr_weatherStation);
    ~MobilePhoneObserver();

    void update();

private:
    WeatherStationObservable *weatherStation;
    int m_temperature;
    int m_id;
};


#endif //DESIGN_PATTERNS_MOBILEPHONEOBSERVER_H
