//
// Created by sergio on 2/12/18.
//

#include <iostream>

#include "MobilePhoneObserver.h"

MobilePhoneObserver::MobilePhoneObserver(WeatherStationObservable *ptr_weatherStation)
{
    this->weatherStation = ptr_weatherStation;
}


MobilePhoneObserver::~MobilePhoneObserver() = default;


void MobilePhoneObserver::update()
{
    m_temperature = weatherStation->getTemperature();

    std::cout << "The temperature has been updated and is now "
              << m_temperature
              << " C"
              << std::endl;
}


