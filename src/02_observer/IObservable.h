//
// Created by sergio on 2/12/18.
//

#ifndef DESIGN_PATTERNS_IOBSERVABLE_H
#define DESIGN_PATTERNS_IOBSERVABLE_H

#include "IObserver.h"

class IObservable
{
public:
    virtual ~IObservable() = default;
    virtual void add(IObserver* ptr_observer) = 0;
    virtual void remove(IObserver* ptr_observer) = 0;
    virtual void notify() = 0;
};


#endif //DESIGN_PATTERNS_IOBSERVABLE_H
