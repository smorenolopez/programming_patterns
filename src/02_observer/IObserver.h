//
// Created by sergio on 2/12/18.
//

#ifndef DESIGN_PATTERNS_IOBSERVER_H
#define DESIGN_PATTERNS_IOBSERVER_H


class IObserver
{
public:
    virtual ~IObserver() = default;

    virtual void update() = 0;
};

#endif //DESIGN_PATTERNS_IOBSERVER_H
