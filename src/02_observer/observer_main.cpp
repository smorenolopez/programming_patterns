


#include "WeatherStationObservable.h"
#include "MobilePhoneObserver.h"

int main()
{
    WeatherStationObservable station;
    MobilePhoneObserver phoneDisplay(&station);
    std::vector<int> temperatures {14, 30, 17, 19, 25};

    station.add(&phoneDisplay);

    for (auto temperature : temperatures)
    {
        station.setTemperature(temperature);
        station.notify();
    }

    return 0;
}