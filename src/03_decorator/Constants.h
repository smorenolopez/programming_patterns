//
// Created by sergio on 6/12/18.
//

#ifndef DESIGN_PATTERNS_CONSTANTS_H
#define DESIGN_PATTERNS_CONSTANTS_H


namespace BEVERAGE
{
    // Beverages constants
    constexpr double EXPRESSO_PRICE = 1.5;

    // Addon constants
    constexpr double CARAMEL_PRICE = 0.5;
    constexpr double EXTRA_SHOT_PRICE = 0.25;
}


#endif //DESIGN_PATTERNS_CONSTANTS_H
