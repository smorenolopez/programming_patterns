//
// Created by sergio on 6/12/18.
//

#ifndef DESIGN_PATTERNS_EXPRESSO_H
#define DESIGN_PATTERNS_EXPRESSO_H


#include "Beverage.h"

class Expresso : public Beverage
{
public:
    Expresso() = default;
    ~Expresso() override = default;

    double cost() override;
};


#endif //DESIGN_PATTERNS_EXPRESSO_H
