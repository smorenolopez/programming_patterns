//
// Created by sergio on 6/12/18.
//

#ifndef DESIGN_PATTERNS_BEVERAGE_H
#define DESIGN_PATTERNS_BEVERAGE_H

class Beverage
{
public:
    virtual ~Beverage() = default;
    virtual double cost() = 0;
};

#endif //DESIGN_PATTERNS_BEVERAGE_H
