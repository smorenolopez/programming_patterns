
#include<iostream>
#include "Beverages/Beverage.h"
#include "Beverages/Expresso.h"
#include "Addons/AddonCaramel.h"
#include "Addons/AddonExtraShots.h"


int main()
{
    Expresso expresso;

    AddonCaramel    expressoWithCaramel(&expresso);
    AddonExtraShots expressoWithExtraShot(&expresso, 1);
    AddonExtraShots expressoWithCaramelWithTwoExtraShots(&expressoWithCaramel, 2);

    std::cout << "The cost of a expresso is: "
              << expresso.cost() << "\n"
              << "The cost of a expresso with caramel is: "
              << expressoWithCaramel.cost() << "\n"
              << "The cost of a expresso and 1 extra shot is: "
              << expressoWithExtraShot.cost() << "\n"
              << "The cost of a expresso with caramel and 2 extra shot is: "
              << expressoWithCaramelWithTwoExtraShots.cost() << "\n";

    return 0;
}
