//
// Created by sergio on 6/12/18.
//

#ifndef DESIGN_PATTERNS_ADDONDECORATOR_H
#define DESIGN_PATTERNS_ADDONDECORATOR_H


#include "../Beverages/Beverage.h"

class AddonDecorator : public Beverage
{
public:
    ~AddonDecorator() override = default;
    virtual double cost() = 0;
};

#endif //DESIGN_PATTERNS_ADDONDECORATOR_H
