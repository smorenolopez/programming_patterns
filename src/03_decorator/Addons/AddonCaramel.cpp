//
// Created by sergio on 6/12/18.
//

#include "AddonCaramel.h"
#include "../Constants.h"

AddonCaramel::AddonCaramel(Beverage *beverage)
        : m_beverage(beverage)
{
}

double AddonCaramel::cost()
{
    return this->m_beverage->cost() + BEVERAGE::CARAMEL_PRICE;
}
