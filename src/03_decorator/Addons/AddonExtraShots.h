//
// Created by sergio on 6/12/18.
//

#ifndef DESIGN_PATTERNS_ADDONEXTRASHOTS_H
#define DESIGN_PATTERNS_ADDONEXTRASHOTS_H


#include "AddonDecorator.h"

class AddonExtraShots : public AddonDecorator
{
public:
    AddonExtraShots(Beverage *beverage,
                    double numOfShots);

    double cost() override;

private:
    Beverage *m_beverage;
    double m_numOfShots;
};


#endif //DESIGN_PATTERNS_ADDONEXTRASHOTS_H
