//
// Created by sergio on 6/12/18.
//

#ifndef DESIGN_PATTERNS_ADDONCARAMEL_H
#define DESIGN_PATTERNS_ADDONCARAMEL_H


#include "AddonDecorator.h"

class AddonCaramel : public AddonDecorator
{
public:
    AddonCaramel(Beverage *beverage);
    ~AddonCaramel() override = default;

    double cost() override;

private:
    Beverage *m_beverage;
};


#endif //DESIGN_PATTERNS_ADDONCARAMEL_H
