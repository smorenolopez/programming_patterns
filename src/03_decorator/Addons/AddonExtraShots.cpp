//
// Created by sergio on 6/12/18.
//

#include "AddonExtraShots.h"
#include "../Constants.h"

AddonExtraShots::AddonExtraShots(Beverage *beverage,
                                 double numOfShots)
    : m_beverage(beverage),
      m_numOfShots(numOfShots)
{
}

double AddonExtraShots::cost()
{
    return this->m_beverage->cost() + BEVERAGE::EXTRA_SHOT_PRICE * m_numOfShots;
}
