#include <iostream>
#include "src/01_strategy/FlyWithWingsBehaviour.h"
#include "src/01_strategy/Duck.h"

class A;


int main()
{
    FlyWithWingsBehaviour flyBehaviour;
    flyBehaviour.set( "Flying with wings right now...");
    flyBehaviour.fly();

    Duck duck(flyBehaviour);
    duck.fly();




    int a;

    //std::cout << b << std::endl;

    return 0;
}

